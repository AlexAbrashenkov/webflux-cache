package com.example.webfluxcache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
public class WebfluxCacheApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebfluxCacheApplication.class, args);
    }

}
