package com.example.webfluxcache.repository;

import com.example.webfluxcache.entity.Auth;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface AuthRepository extends ReactiveCrudRepository<Auth, Integer> {

    Flux<Auth> findAllByDeletedFalse();
}
