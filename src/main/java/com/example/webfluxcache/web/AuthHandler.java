package com.example.webfluxcache.web;

import com.example.webfluxcache.service.AuthService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
public class AuthHandler {

    private final AuthService authService;

    public @NonNull Mono<ServerResponse> createAuth(ServerRequest serverRequest) {
        Integer userId = serverRequest.queryParam("user_id")
                .map(Integer::valueOf)
                .orElseThrow();

        return authService.createAuth(userId)
                .flatMap(auth -> ServerResponse.ok().bodyValue(auth));
    }

    public @NonNull Mono<ServerResponse> deleteAuth(ServerRequest serverRequest) {
        int authId = Integer.parseInt(serverRequest.pathVariable("id"));
        return authService.deleteAuth(authId)
                .flatMap(auth -> ServerResponse.ok().bodyValue(auth));
    }

    public @NonNull Mono<ServerResponse> findById(ServerRequest serverRequest) {
        int userId = Integer.parseInt(serverRequest.pathVariable("id"));
        return authService.findById(userId)
                .flatMap(auth -> ServerResponse.ok().bodyValue(auth));
    }

    public @NonNull Mono<ServerResponse> findAllFromCache(ServerRequest serverRequest) {
        return authService.findAllFromCache()
                .collectList()
                .flatMap(auths -> ServerResponse.ok().bodyValue(auths));
    }

    public @NonNull Mono<ServerResponse> loadAll(ServerRequest serverRequest) {
        return authService.loadAll()
                .collectList()
                .flatMap(auths -> ServerResponse.ok().bodyValue(auths));
    }
}
