package com.example.webfluxcache.web;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RouterFunctions.*;

@Configuration
@AllArgsConstructor
public class RestConfiguration {

    private final AuthHandler authHandler;

    @Bean
    public RouterFunction<ServerResponse> auth() {
        return route().POST("/auth", authHandler::createAuth).build()
                .and(route().DELETE("/auth/{id}", authHandler::deleteAuth).build())
                .and(route().GET("/auth", authHandler::findAllFromCache).build())
                .and(route().GET("/auth/load", authHandler::loadAll).build())
                .and(route().GET("/auth/{id}", authHandler::findById).build());
    }
}
