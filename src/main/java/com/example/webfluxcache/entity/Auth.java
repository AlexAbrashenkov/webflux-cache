package com.example.webfluxcache.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.time.LocalDateTime;

@Data
@With
@Table(name = "auths", schema = "test")
@AllArgsConstructor
@NoArgsConstructor
public class Auth {

    @Id
    @Column("id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column("user_id")
    @JsonProperty("user_id")
    private int userId;

    @Column("created_at")
    @JsonProperty("created_at")
    @CreatedDate
    private LocalDateTime createdAt;

    @Column("updated_at")
    @JsonProperty("updated_at")
    @LastModifiedDate
    private LocalDateTime updatedAt;

    @Column("deleted_at")
    @JsonProperty("deleted_at")
    private LocalDateTime deletedAt;

    @Column("deleted")
    @JsonProperty("deleted")
    private boolean deleted;
}
