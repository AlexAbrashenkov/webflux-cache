package com.example.webfluxcache.service;

import com.example.webfluxcache.entity.Auth;
import com.example.webfluxcache.repository.AuthRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.github.benmanes.caffeine.cache.AsyncCache;
import com.github.benmanes.caffeine.cache.Caffeine;
import io.r2dbc.postgresql.api.Notification;
import io.r2dbc.postgresql.api.PostgresqlConnection;
import io.r2dbc.spi.ConnectionFactory;
import io.r2dbc.spi.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static java.time.temporal.ChronoField.*;

@Slf4j
@Service
public class AuthService {

    private final static int MAX_RETRY_ATTEMPTS = 10;
    private final static int MIN_RETRY_DELAY_MS = 100;
    private final static double RETRY_DELAY_MULTIPLIER = 1.5;

    private final AuthRepository authRepository;
    private final ConnectionFactory connectionFactory;
    private PostgresqlConnection connection;
    private Disposable notificationsDisposable;
    private final ObjectMapper objectMapper;
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    private int attempt = 1;
    private long delay = MIN_RETRY_DELAY_MS;
    private boolean isRecovering = false;

    private final AsyncCache<Integer, Auth> cache = Caffeine.newBuilder()
            .expireAfterAccess(Duration.ofMinutes(5))
            .buildAsync();

    public AuthService(AuthRepository authRepository,
                       ConnectionFactory connectionFactory) {
        this.authRepository = authRepository;
        this.connectionFactory = connectionFactory;

        objectMapper = new ObjectMapper();
        final DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .appendPattern("yyyy-MM-dd'T'HH:mm:ss.")
                .appendValue(NANO_OF_SECOND)
                .toFormatter();
        final LocalDateTimeDeserializer deserializer = new LocalDateTimeDeserializer(formatter);
        final JavaTimeModule javaTimeModule = new JavaTimeModule();
        javaTimeModule.addDeserializer(LocalDateTime.class, deserializer);
        objectMapper.registerModule(javaTimeModule);
    }

    @PostConstruct
    private void postConstruct() {
        subscribeToNotificationsBlocking();
    }

    @PreDestroy
    private void preDestroy() {
        connection.close().subscribe();
    }

    @Transactional
    public Mono<Auth> createAuth(int userId) {
        var auth = new Auth().withUserId(userId).withCreatedAt(LocalDateTime.now());
        return authRepository.save(auth)
                .doOnNext(auth1 -> log.info("Auth saved {}", auth1))
                .doOnNext(auth1 -> cache.put(auth1.getId(), CompletableFuture.completedFuture(auth1)));
    }

    @Transactional
    public Mono<Auth> deleteAuth(int authId) {
        return authRepository.findById(authId)
                .map(auth -> auth.withDeleted(true).withDeletedAt(LocalDateTime.now()))
                .flatMap(authRepository::save)
                .doOnNext(auth -> log.info("Auth deleted {}", auth))
                .doOnNext(auth -> cache.synchronous().invalidate(auth.getId()));
    }

    public Mono<Auth> findById(int authId) {
        log.info("Find auth by id {}", authId);
        CompletableFuture<Auth> authFuture = cache.getIfPresent(authId);
        if (authFuture != null) {
            log.info("Found auth in cache");
            return Mono.fromFuture(authFuture);
        }
        log.info("Loading auth from db by id {}", authId);
        return authRepository.findById(authId)
                .doOnNext(auth -> {
                    if (!isRecovering) {
                        cache.put(auth.getId(), CompletableFuture.completedFuture(auth));
                    }
                });
    }

    public Flux<Auth> findAllFromCache() {
        log.info("Find all auths");
        return Flux.fromIterable(cache.asMap().entrySet())
                .flatMap(entry -> Mono.fromFuture(entry.getValue()));
    }

    public Flux<Auth> loadAll() {
        log.info("Load all to cache");
        return authRepository.findAllByDeletedFalse()
                .doOnNext(auth -> {
                    if (!isRecovering) {
                        cache.put(auth.getId(), CompletableFuture.completedFuture(auth));
                    }
                });
    }

    private void subscribeToNotificationsBlocking() {
        log.info("Subscribing to DB notifications on auth update");
        try {
            connection = Mono.from(connectionFactory.create())
                    .cast(PostgresqlConnection.class)
                    .block();
            connection.createStatement("LISTEN auth_delete_notification").execute()
                    .flatMap(Result::getRowsUpdated)
                    .subscribe();
            notificationsDisposable = connection.getNotifications()
                    .doOnError(this::handleError)
                    .subscribe(this::processUpdateNotification);
            log.info("Successfully subscribed to auth update notifications");
            isRecovering = false;
            attempt = 1;
            delay = MIN_RETRY_DELAY_MS;
        } catch (Exception e) {
            log.warn("Subscribing attempt {}/{} failed", attempt, MAX_RETRY_ATTEMPTS);
            if (attempt < MAX_RETRY_ATTEMPTS) {
                attempt++;
                delay *= RETRY_DELAY_MULTIPLIER;
                executorService.schedule(this::subscribeToNotificationsBlocking, delay, TimeUnit.MILLISECONDS);
            } else {
                log.error("Subscribing attempts {}/{} were exhausted", attempt, MAX_RETRY_ATTEMPTS);
            }
        }
    }

    private void handleError(Throwable throwable) {
        log.info("Handling error");
        if (!isRecovering) {
            log.info("Recovering from error...");
            isRecovering = true;
            cache.synchronous().invalidateAll();
            executorService.execute(() -> {
                log.info("Disposing...");
                notificationsDisposable.dispose();
                connection.close();
                executorService.schedule(this::subscribeToNotificationsBlocking, delay, TimeUnit.MILLISECONDS);
            });
        }
    }

    private void processUpdateNotification(Notification notification) {
        log.info("Notification: {}", notification);
        try {
            Auth deletedAuth = objectMapper.readValue(notification.getParameter(), Auth.class);
            if (deletedAuth.isDeleted()) {
                cache.synchronous().invalidate(deletedAuth.getId());
                log.info("Auth with id {} removed from cache by trigger", deletedAuth.getId());
            }
        } catch (JsonProcessingException e) {
            log.error("Failed to parse updated row {}", notification.getParameter(), e);
        }
    }
}
