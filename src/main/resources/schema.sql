CREATE TABLE auths
(
    id         SERIAL NOT NULL
    user_id    INTEGER NOT NULL,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP,
    deleted    BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (id, deleted)
) PARTITION BY LIST (deleted);

CREATE TABLE auths_active
    PARTITION OF auths
    FOR VALUES IN (false);

CREATE TABLE auths_archive
    PARTITION OF auths
    FOR VALUES IN (true);

CREATE OR REPLACE FUNCTION notify_event() RETURNS TRIGGER AS
$$
DECLARE
payload JSON;
BEGIN
    payload = row_to_json(NEW);
    PERFORM pg_notify('auth_delete_notification', payload::text);
RETURN NULL;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS notify_auth_delete_event ON auths_archive;

CREATE TRIGGER notify_auth_delete_event
    AFTER INSERT
    ON auths_archive
    FOR EACH ROW
    EXECUTE PROCEDURE notify_event();